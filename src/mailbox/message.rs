/// A Message is the interface to data that can be sent between actors.
/// Messages and their return types must be `Send` so that the actors can be executed concurrently
/// on separate threads.
pub trait Message: Send {
    type Return: Send;
}
