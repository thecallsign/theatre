use futures::channel::oneshot::Canceled;
use thiserror::Error;
use tokio::sync::mpsc::error::{SendError, TrySendError};

/// Possible error variants.
///
/// At the moment, there is only one kind of theatre specific error.
/// A `SendError` is used when messages have failed to reach to a mailbox.
#[derive(Error, Debug)]
pub enum TheatreError {
    #[error("Failed to send message")]
    SendError,
    #[error("Mailbox is full")]
    MailboxFull,
    #[error("Mailbox is closed")]
    MailboxClosed,
}

impl<T> From<TrySendError<T>> for TheatreError {
    fn from(error: TrySendError<T>) -> Self {
        match error {
            TrySendError::Full(_) => { TheatreError::MailboxFull }
            TrySendError::Closed(_) => { TheatreError::MailboxClosed }
        }
    }
}

impl<M> From<SendError<M>> for TheatreError {
    fn from(_error: SendError<M>) -> Self {
        TheatreError::SendError
    }
}

impl From<Canceled> for TheatreError {
    fn from(_: Canceled) -> Self {
        TheatreError::MailboxClosed
    }
}
