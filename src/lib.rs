//! Theatre: A concise async actor model implementation
//!
//! Example:
//! ```rust
//! use theatre::prelude::*;
//!
//! #[derive(Default)]
//! struct Accumulator {
//!     count: i32,
//! }
//!
//! impl Actor for Accumulator {}
//!
//! // A message to increase the count of the accumulator by one
//! struct Increment;
//! impl Message for Increment {
//!     type Return = ();
//! }
//!
//! // A message to get the current count of the accumulator
//! struct GetCount;
//! impl Message for GetCount {
//!     type Return = i32;
//! }
//!
//! // Handle `Increment` messages for the accumulator
//! #[async_trait]
//! impl Handler<Increment> for Accumulator {
//!     async fn handle(
//!         &mut self,
//!         _msg: Increment,
//!         _context: &mut Context<Self>,
//!     ) -> <Increment as Message>::Return {
//!         println!("Adding 1");
//!         self.count += 1
//!     }
//! }
//!
//! // Handle `GetCount`
//! #[async_trait]
//! impl Handler<GetCount> for Accumulator {
//!     async fn handle(
//!         &mut self,
//!         _msg: GetCount,
//!         _context: &mut Context<Self>,
//!     ) -> <GetCount as Message>::Return {
//!         self.count
//!     }
//! }
//!
//! #[tokio::main]
//! async fn main() {
//!     let mut acc = Accumulator::default().spawn().await;
//!
//!     assert_eq!(0, acc.send(GetCount).await.unwrap());
//!
//!     acc.send(Increment).await.unwrap(); // Output: `Adding 1`
//!     acc.send(Increment).await.unwrap(); // Output: `Adding 1`
//!     acc.send(Increment).await.unwrap(); // Output: `Adding 1`
//!     acc.send(Increment).await.unwrap(); // Output: `Adding 1`
//!
//!     println!("Value: {}", acc.send(GetCount).await.unwrap()); // Output: Value: 4
//!     assert_eq!(4, acc.send(GetCount).await.unwrap());
//! }
//! ```

mod actor;
pub mod actors;
mod context;
mod error;
mod handler;
pub mod mailbox;

pub use actor::Config;
pub use actor::{start, Actor, ActorExt, ActorHandle};
pub use context::Context;
pub use error::TheatreError;
pub use handler::Handler;

pub mod prelude {
    //! Convenience module for common actor types
    pub use super::actor::{Actor, ActorExt};
    pub use super::context::Context;
    pub use super::error::TheatreError;
    pub use super::handler::Handler;
    pub use super::mailbox::{Addr, Message, Recipient};
    pub use async_trait::async_trait;
}
