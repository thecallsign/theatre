use crate::actor::Actor;
use crate::context::Context;
use crate::mailbox::Message;
use async_trait::async_trait;

#[async_trait]
/// The `Handler` trait allows for an actor to have a per-message type handle implementation.
///
/// Handler is an `async_trait` so implementors must attribute their implantation with `#[async_trait]`
pub trait Handler<M>
where
    Self: Sized + Actor,
    M: Message,
    M::Return: Send,
{
    async fn handle(&mut self, msg: M, context: &mut Context<Self>) -> M::Return;
}
