use theatre::prelude::*;

#[derive(Default)]
struct Accumulator {
    count: i32,
}

impl Actor for Accumulator {}

struct Increment;

impl Message for Increment {
    type Return = ();
}

#[async_trait]
impl Handler<Increment> for Accumulator {
    async fn handle(
        &mut self,
        _msg: Increment,
        _context: &mut Context<Self>,
    ) -> <Increment as Message>::Return {
        println!("Adding 1");
        self.count += 1
    }
}

#[tokio::main]
async fn main() {
    let acc = Accumulator::default().spawn().await;
    acc.send(Increment).await.unwrap();

    let _rawr = acc
        .execute(|actor| {
            actor.count += 5;
            "return"
        })
        .await
        .unwrap();

    let _rawr = acc
        .execute(|actor| {
            actor.count += 5;
            "return"
        })
        .await
        .unwrap();

    // let mut acc2 = acc.clone();
    // let rawr = acc2.execute(move |actor| {
    //     acc.execute_async::<_, usize>(async move {
    //         println!("Value nested: {}", actor.count);
    //         12
    //     });
    // }).await.unwrap();
}
